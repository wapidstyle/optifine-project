# Optifine Project
### What the heck is the Optifine Project?
The Optifine Project (to save myself typing, code-named `op`) is a attempt to remodel the
Optifine website, avalible at [optifine.net](optifine.net). This version is avalible at
[wapidstyle.github.io/optifine-project](wapidstyle.github.io/optifine-project).
### What is Optifine?
Optifine is a Minecraft mod intending to improve a player's framerate. It also allows for
more features, such as zoom and redoing chunk loading. It is said to double a player's
framerate.
## How is it going to be fixed?
I'll show you an image.

![Uses JS, HTML5 and CSS3](http://www.w3.org/html/logo/badge/html5-badge-h-css3-performance.png)

# Contributing
If you would like to contribute, make a pull request.
